using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Model;

namespace XamPetDroid.Service.Storage
{
    public interface IPetDb
    {
        int CreatePet(Pet p);

        Pet ReadPet(int index);

        int UpdatePet(Pet p);

        void DeletePet(Pet p);

        void DeletePet(int p);

        int Count();
    }
}