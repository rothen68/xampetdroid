using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Model;

namespace XamPetDroid.Service.Storage
{
    public class FakePetDb : IPetDb
    {
        private static readonly List<Pet> _fakeDb = new List<Pet>();

        public int CreatePet(Pet p)
        {
            p.Id = _fakeDb.Count;
            _fakeDb.Add(p);
            return p.Id;
        }

        public int UpdatePet(Pet p)
        {
            _fakeDb.RemoveAt(p.Id);
            _fakeDb.Insert(p.Id, p);
            return p.Id;
        }

        public Pet ReadPet(int index)
        {
            return _fakeDb[index];
        }

        public void DeletePet(Pet p)
        {
            _fakeDb.Remove(p);
        }

        public void DeletePet(int p)
        {
            _fakeDb.RemoveAt(p);
        }

        public int Count()
        {
            return _fakeDb.Count;
        }
    }
}