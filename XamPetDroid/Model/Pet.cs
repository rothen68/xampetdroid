using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Object = Java.Lang.Object;

namespace XamPetDroid.Model

{
    public class Pet : Object
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string About { get; set; }
    }
}