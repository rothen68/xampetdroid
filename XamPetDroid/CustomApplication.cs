using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Service.Storage;

namespace XamPetDroid
{
    [Application]
    public class CustomApplication : Application
    {
        public IPetDb petDb;

        public CustomApplication(IntPtr ptr, JniHandleOwnership handleOwnership)
        {
            Initialize();
        }

        public CustomApplication()
        {
            Initialize();
        }

        private void Initialize()
        {
            petDb = new FakePetDb();
        }
    }
}