﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using XamPetDroid.Model;

namespace XamPetDroid.Controller
{
    [Activity(Label = "XamPetDroid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : BaseActivity
    {
        private ListView lstPets;
        private Button btnAdd;

        private PetsListAdapter _adapter;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.main_layout);

            // Get our button from the layout resource,
            // and attach an event to it
            btnAdd = FindViewById<Button>(Resource.Id.main_btnAdd);
            lstPets = FindViewById<ListView>(Resource.Id.main_lstListView);

            btnAdd.Click += BtnAddOnClick;

            _adapter = new PetsListAdapter(PetDb, this);

            lstPets.Adapter = _adapter;
            lstPets.ItemClick += LstPets_ItemClick;
        }

        private void LstPets_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            GoToDetailView(e.Position);
        }

        protected override void OnResume()
        {
            base.OnResume();
            _adapter.NotifyDataSetChanged();
        }

        private void BtnAddOnClick(object sender, EventArgs eventArgs)
        {
            int newPetId = PetDb.CreatePet(new Pet() { Name = "New pet" });
            _adapter.NotifyDataSetChanged();
            GoToDetailView(newPetId);
        }

        private void GoToDetailView(int petId)
        {
            var intent = new Intent(this, typeof(DetailActivity));
            intent.PutExtra("PetId", petId);
            StartActivity(intent);
        }
    }
}