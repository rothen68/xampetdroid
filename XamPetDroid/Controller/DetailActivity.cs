using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Model;

namespace XamPetDroid.Controller
{
    [Activity(Label = "DetailActivity")]
    public class DetailActivity : BaseActivity
    {
        private EditText _petName;
        private EditText _petAge;
        private EditText _petAbout;

        private Button _btnSave;
        private Button _btnDelete;
        private Button _btnBack;

        private Pet _currentPet;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.detail_layout);

            var bundle = this.Intent.Extras;
            int petId = -1;
            if (bundle != null)
            {
                petId = bundle.GetInt("PetId", -1);
            }

            _currentPet = PetDb.ReadPet(petId);

            _petName = FindViewById<EditText>(Resource.Id.detail_etxtName);
            _petName.Text = _currentPet.Name;

            _petAbout = FindViewById<EditText>(Resource.Id.detail_etxtAbout);
            _petAbout.Text = _currentPet.About;

            _petAge = FindViewById<EditText>(Resource.Id.detail_etxtAge);
            _petAge.Text = _currentPet.Age.ToString();

            _btnSave = FindViewById<Button>(Resource.Id.detail_btnSave);
            _btnDelete = FindViewById<Button>(Resource.Id.detail_btnDelete);
            _btnBack = FindViewById<Button>(Resource.Id.detail_btnBack);

            _btnSave.Click += _btnSave_Click;
            _btnDelete.Click += _btnDelete_Click;
            _btnBack.Click += _btnBack_Click;
        }

        private void _btnBack_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void _btnDelete_Click(object sender, EventArgs e)
        {
            PetDb.DeletePet(_currentPet.Id);
            Finish();
        }

        private void _btnSave_Click(object sender, EventArgs e)
        {
            _currentPet.Name = _petName.Text;
            _currentPet.Age = Int32.Parse(_petAge.Text);
            _currentPet.About = _petAbout.Text;
            PetDb.UpdatePet(_currentPet);
        }
    }
}