using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Service.Storage;

namespace XamPetDroid.Controller
{
    public class BaseActivity : Activity
    {
        protected Application app;

        protected IPetDb PetDb
        {
            get
            {
                return app != null ? ((CustomApplication)Application).petDb : null;
            }
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            app = Application;
        }
    }
}