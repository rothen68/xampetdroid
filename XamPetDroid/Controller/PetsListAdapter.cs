using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XamPetDroid.Service.Storage;
using Object = Java.Lang.Object;

namespace XamPetDroid.Controller
{
    public class PetsListAdapter : BaseAdapter
    {
        private IPetDb _petDb;
        private Activity _context;

        public PetsListAdapter(IPetDb petDb, Activity context)
        {
            _petDb = petDb;
            _context = context;
        }

        public override Object GetItem(int position)
        {
            return _petDb.ReadPet(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (convertView == null)
            {
                convertView = _context.LayoutInflater.Inflate(Resource.Layout.PetsListItem_layout, null);
            }
            var currentPet = _petDb.ReadPet(position);

            convertView.FindViewById<TextView>(Resource.Id.PetsListItem_txtName).Text = currentPet.Name;
            convertView.FindViewById<TextView>(Resource.Id.PetsListItem_txtAge).Text = currentPet.Age.ToString();
            return convertView;
        }

        public override int Count
        {
            get
            {
                return _petDb.Count();
            }
        }
    }
}